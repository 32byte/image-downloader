# Image downloader

## Requirements
This shell script requires wget.

## Usage
```bash
sh downloader.sh number_of_images delay
# or
chmod +x downloader.sh
./downloader.sh number_of_images delay
```