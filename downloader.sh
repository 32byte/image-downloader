#!/bin/bash

echo " _____                    _                 _             "
echo "(____ \                  | |               | |            "
echo " _   \ \ ___  _ _ _ ____ | | ___   ____  _ | | ____  ____ "
echo "| |   | / _ \| | | |  _ \| |/ _ \ / _  |/ || |/ _  )/ ___)"
echo "| |__/ / |_| | | | | | | | | |_| ( ( | ( (_| ( (/ /| |    "
echo "|_____/ \___/ \____|_| |_|_|\___/ \_||_|\____|\____)_|    "
echo "                                                          "
echo "By Alexey Lebedenko"
echo ""

# Usage
if [ "$#" -ne 2 ]; then
    echo "Usage: ./downloader.sh number delay"
    exit -1
fi


# create images directory, if doesn't exist
if [ ! -d "images" ]; then
    mkdir "images"
fi

# download 100'000 images
for i in $( eval echo {0..$1} )
do
    echo "Downloading image $i/$1!"
    wget 'https://www.thispersondoesnotexist.com/image' -O images/image$i.png
    echo "Waiting $2s"
    sleep $2
done